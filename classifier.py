#!/usr/bin/env python3

import os
import os.path
import argparse

import csv
import matplotlib.image as img
from collections import Counter
import numpy as np
import copy
import math


def setup_arg_parser():
    parser = argparse.ArgumentParser(description='Learn and classify image data.')
    parser.add_argument('train_path', type=str, help='path to the training data directory')
    parser.add_argument('test_path', type=str, help='path to the testing data directory')
    mutex_group = parser.add_mutually_exclusive_group(required=True)
    mutex_group.add_argument('-k', type=int,
                             help='run k-NN classifier (if k is 0 the code may decide about proper K by itself')
    mutex_group.add_argument("-b",
                             help="run Naive Bayes classifier", action="store_true")
    parser.add_argument("-o", metavar='filepath',
                        default='classification.dsv',
                        help="path (including the filename) of the output .dsv file with the results")
    return parser


def most_common(array):
    data = Counter(array)
    return max(array, key=data.get)


def load_images(path, train):
    path = path + '/'
    labels = []
    files = os.listdir(path)
    if train:
        files.remove('truth.dsv')
        dsvf = path + 'truth.dsv'
        with open(dsvf) as dsvfile:
            reader = csv.reader(dsvfile, delimiter=':')
            truth = {row[0]: row[1] for row in reader}
    vectors = []
    for file in files:
        if '.png' in file:
            if train:
                labels.append(truth[file])
            img_data = img.imread(path + file)
            if len(img_data.shape) > 2:         # check if greyscale, if not -> convert
                img_data = img_data[:, :, 0]
            vectors.append(flatten(img_data))
    vectors = np.array(vectors)
    return files, vectors, labels


def flatten(array):
    return [item for sublist in array for item in sublist]


def dist(x_vector, y_vector):       # euclidean distance of two vectors
    diff = x_vector - y_vector
    return np.linalg.norm(diff)


def compare_test_vector(test_vector, train_vectors):
    return [dist(test_vector, sample) for sample in train_vectors]


def knn(train_vectors, train_labels, test_vectors, test_labels, k):
    for test_sample in test_vectors:        # compare all test vectors to training vectors and find the closest match
        sample_distances = compare_test_vector(test_sample, train_vectors)
        sorted_sample_distances = sorted(enumerate(sample_distances), key=lambda x: x[1])
        closest = [train_labels[idx[0]] for idx in sorted_sample_distances[:k]]
        test_labels.append(most_common(closest))
    return test_labels


def bayes_training(train_vectors, train_labels, rnd):
    labels_probs = {}           # probability for each label, updates during algorithm
    label_prior_probs = {}      # probability of each label before algorithm
    pixel_probs = []            # for each pixel -> count for each value
    for label in train_labels:
        if label not in labels_probs:
            labels_probs[label] = 1
            label_prior_probs[label] = 1
        else:
            label_prior_probs[label] += 1
    probs_table = []
    for j in range(len(train_vectors[0])):      # vector[pixel][value]{label:probability}
        labels_temp = []
        pixel_temp = []
        for k in range(rnd + 1):
            labels_temp.append(copy.deepcopy(labels_probs))
            pixel_temp.append(1)
        pixel_probs.append(pixel_temp)
        probs_table.append(labels_temp)

    for i in range(len(train_vectors)):
        for j in range(len(train_vectors[0])):
            train_vectors[i][j] = int(round(train_vectors[i][j] * rnd))    # sharpen image
            for k in range(rnd + 1):
                if train_vectors[i][j] == k:
                    probs_table[j][k][train_labels[i]] += 1
                    pixel_probs[j][k] += 1

    for label in label_prior_probs:
        label_prior_probs[label] /= (len(train_labels))

    return probs_table, pixel_probs, label_prior_probs


def bayes_testing(probs_table, test_vectors, test_labels, pixel_probs, label_prior_probs, rnd):
    # for every img -> for every pixel -> for every label = probability of value => most probable = label
    for i in range(len(test_vectors)):
        possible_labels = {label: 0 for label in label_prior_probs}
        for j in range(len(test_vectors[i])):
            val = int(round(test_vectors[i][j] * rnd))  # sharpen image
            for key in probs_table[j][val]:         # calculate probability for each pixel and add them together
                possible_labels[key] += math.log((probs_table[j][val][key] / pixel_probs[j][val]) * label_prior_probs[key])
        test_labels.append(max(possible_labels, key=possible_labels.get))     # pick label with max probability and save
    return test_labels


def main():
    parser = setup_arg_parser()
    args = parser.parse_args()

    # KNN call: python classifier.py -k [knn constant] -o [dsv file to save results] [training data] [test data]
    # Bayes call: python classifier.py -b -o [dsv file to save results] [training data] [test data]

    train_files, train_vectors, train_labels = load_images(args.train_path, True)
    test_files, test_vectors, test_labels = load_images(args.test_path, False)

    if args.k:
        if args.k == 0:
            k = 3
        else:
            k = args.k
        knn(train_vectors, train_labels, test_vectors, test_labels, k)
    elif args.b:
        probs_table, pixel_probs, label_prior_probs = bayes_training(train_vectors, train_labels, 10)
        bayes_testing(probs_table, test_vectors, test_labels, pixel_probs, label_prior_probs, 10)

    dsvfile = open(args.o, 'w', newline='')
    writer = csv.writer(dsvfile, delimiter=':')
    for i in range(len(test_labels)):
        writer.writerow([test_files[i], test_labels[i]])
    dsvfile.close()


if __name__ == '__main__':
    main()
